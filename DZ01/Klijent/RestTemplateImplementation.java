package hr.fer.rassus.klijent.resttemplate;

import java.util.LinkedList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import hr.fer.rassus.klijent.RestInterface;
import hr.fer.rassus.klijent.model.Measurement;
import hr.fer.rassus.klijent.model.SensorDescription;
import hr.fer.rassus.klijent.model.UserAddress;

public class RestTemplateImplementation implements RestInterface {

	private String baseURL;
	private RestTemplate restTemplate;

	public RestTemplateImplementation(String url) {
		this.baseURL = url;

        restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
	}

	@Override
	public List<SensorDescription> getListOfSensors() {
	    List<SensorDescription> courseList = restTemplate.getForObject(
	    		baseURL + "/getSensors",
	    		SensorDescriptionList.class);
	    return courseList;
	}

	public static class SensorDescriptionList extends LinkedList<SensorDescription> {}

	@Override
	public void register(SensorDescription sensorDescription) {
		ResponseEntity<String> response = restTemplate.postForEntity(baseURL + "/newSensor", sensorDescription, String.class);
		System.out.println(response);
//		String locationPath = response.getHeaders().getLocation().getRawPath();
//		System.out.println(locationPath);
//		return Integer.parseInt(locationPath.substring(locationPath.lastIndexOf('/') + 1));
	}

	@Override
	public SensorDescription getSensorDescription(int id) {
		return restTemplate.getForObject(baseURL + "/getSensors",
	    		SensorDescriptionList.class).get(id);
	}

	@Override
	public int getSensorDescriptionId(String username) {
		int id = 0;
		for (SensorDescription sd : restTemplate.getForObject(baseURL + "/getSensors",
	    		SensorDescriptionList.class)) {
			if (sd.getUsername().equals(username)) id = sd.getId();
		}
		return id;
	}
	
	@Override
	public void storeMeasurement(Measurement measurement) {
//		ResponseEntity<String> response = 
		restTemplate.postForEntity(baseURL + "/storeMeasurement", measurement, String.class);
//		String locationPath = response.getHeaders().getLocation().getRawPath();
//		System.out.println(locationPath);
	}
	
	@Override
	public List<Measurement> getMeasurements() {
		List<Measurement> courseList = restTemplate.getForObject(
	    		baseURL + "/getMeasurements",
	    		MeasurementList.class);
	    return courseList;
	}
	
	public static class MeasurementList extends LinkedList<Measurement> {}

	@Override
	public void deleteSensorDescription(int id) {
		restTemplate.delete(baseURL + "/deleteSensor/" + id);
	}

	@Override
	public UserAddress searchNeighbour(String username) {
		return restTemplate.getForObject(baseURL + "/searchNeighbour/" + username,
				UserAddress.class);
	}

}
