package hr.fer.rassus.klijent;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import hr.fer.rassus.klijent.Worker;

public class MultiThreadedServer implements ServerIf {
	
	private static int port; // server port
	private static final int NUMBER_OF_THREADS = 4;
	//Max queue length for incoming connection requests.
	private static final int BACKLOG = 10;
	private final AtomicInteger activeConnections;
	private ServerSocket serverSocket;
	private final ExecutorService executor;
	private final AtomicBoolean runningFlag;
	private final long start;
	
	public MultiThreadedServer(long start, int port2) {
		this.start = start;
		port = port2;
		activeConnections = new AtomicInteger(0);
		executor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
		runningFlag = new AtomicBoolean(false);
	}

	@Override
	public void startup() {
		// create a server socket, bind it to the specified port
		// on the local host and set the backlog for
		// client requests
		try {
			this.serverSocket = new ServerSocket(port, BACKLOG);
			// set timeout to avoid blocking
			serverSocket.setSoTimeout(500);
			runningFlag.set(true);
			System.out.println("Server is ready!");
		} catch (SocketException e1) {
			System.err.println("Exception caught when setting server socket timeout: " + e1);
		} catch (IOException ex) {
			System.err.println("Exception caught when opening or setting the server socket: " + ex);
		}
	}

	@Override
	public void loop() {
		while(runningFlag.get()) {
			try{
				// create a new socket, accept and listen for a
				//connection to be made to this socket
				Socket clientSocket = serverSocket.accept();
				// execute a tcp request handler in a new thread
				Runnable worker= new Worker(clientSocket, runningFlag, activeConnections, start);
				executor.execute(worker);
				activeConnections.getAndIncrement();
			} catch(SocketTimeoutException ste) {
//				System.out.println("timeout");
				// do nothing, check runningFlag
			} catch(IOException ex) {
				System.err.println("Exception caught when waiting for a connection: " + ex);
			}
		}
	}

	@Override
	public void shutdown() {
		while( activeConnections.get() > 0 ) {
			System.out.println( "WARNING: There are still active connections" );
			try {
				Thread.sleep( 5000 );
			} catch( java.lang.InterruptedException e ){}
		}
		if( activeConnections.get() == 0 ) {
			System.out.println("Starting server shutdown.");
			try {
				serverSocket.close();
			} catch (IOException e) {
				System.err.println("Exception caught when closing the server socket: " + e);
			} finally {
				executor.shutdown();
			}
			System.out.println("Server has been shutdown.");
		}
	}


	@Override
	public AtomicBoolean getRunningFlag() {
		return runningFlag;
	}
}