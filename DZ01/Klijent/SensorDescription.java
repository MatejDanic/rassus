package hr.fer.rassus.klijent.model;


public class SensorDescription {

	private int id;
	private String username;
	private double latitude, longitude;
	private String ipAddress;
	private int port;
	
//	public SensorDescription(String username, double latitude, double longitude, String ipAddress, int port) {
//		this.username = username;
//		this.latitude = latitude;
//		this.longitude = longitude;
//		this.ipAddress = ipAddress;
//		this.port = port;
//	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	@Override
	public String toString() {
		return "SensorDescription [id=" + id + ", username=" + username + ", latitude=" + latitude + ", longitude=" + longitude
				+ ", ipAddress=" + ipAddress + ", port=" + port + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
