package hr.fer.rassus.klijent.model;


public class SensorReading {
	
	private String temperature, pressure, humidity, co2, no2, so2;

	public String getTemperature() {
		return temperature;
	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}

	public String getPressure() {
		return pressure;
	}

	public void setPressure(String pressure) {
		this.pressure = pressure;
	}

	public String getHumidity() {
		return humidity;
	}

	public void setHumidity(String humidity) {
		this.humidity = humidity;
	}

	public String getCo2() {
		return co2;
	}

	public void setCo(String co2) {
		this.co2 = co2;
	}

	public String getNo2() {
		return no2;
	}

	public void setNo2(String no2) {
		this.no2 = no2;
	}

	public String getSo2() {
		return so2;
	}

	public void setSo2(String so2) {
		this.so2 = so2;
	}
	
	@Override
	public String toString() {
		return "SensorReading [temperature=" + temperature + ", pressure=" + pressure + ", humidity="
				+ humidity + ", co2=" + co2 + ", no2=" + no2 + ", so2=" + so2 + "]";
	}
}
