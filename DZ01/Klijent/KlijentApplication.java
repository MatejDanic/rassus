package hr.fer.rassus.klijent;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.opencsv.bean.CsvToBeanBuilder;

import hr.fer.rassus.klijent.model.Measurement;
import hr.fer.rassus.klijent.model.SensorDescription;
import hr.fer.rassus.klijent.model.SensorReading;
import hr.fer.rassus.klijent.model.UserAddress;

@SpringBootApplication
public class KlijentApplication {
	
	private static final double LAT_MAX = 45.75;
	private static final double LAT_MIN = 45.85;
	private static final double LON_MAX = 15.87;
	private static final double LON_MIN = 16.00;
	public static List<SensorReading> readings;
	public static long start_time;
	
	public static void main(String[] args) throws IllegalStateException, FileNotFoundException {
		
		
		readings = new CsvToBeanBuilder<SensorReading>(new FileReader("mjerenja.csv")).withType(SensorReading.class).build().parse();
		Scanner input = new Scanner(System.in);
		while(true) {
			System.out.println("Spoji se - 1\nIskljuci se - 0");
			start_time = System.currentTimeMillis();
			int i = 0;
			try {
				i = input.nextInt();
			} catch (NoSuchElementException e) {
//				e.printStackTrace();
			}
			if (i == 1) {
				String url = "http://localhost:8080/sensors";

				try {
//					runScript(RestFactory.getUrlConnectionImplementation(url));
					runScript(RestFactory.getRestTemplateImplementation(url));
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else if (i == 0) break;
		}
		input.close();
		System.out.println("-----KRAJ-----");
	}

	private static void runScript(RestInterface rest) throws IllegalStateException, IOException {
		System.out.println(rest.getListOfSensors());
		List<SensorDescription> sensors = rest.getListOfSensors();
		Scanner input2 = new Scanner(System.in);
		System.out.println("Upisite username");
		String username = "";
		while(true) {
			username = input2.next();
			boolean exists = false;
			for (SensorDescription sd : sensors) {
				if (sd.getUsername().equals(username)) {
					System.out.println("Username zauzet");
					exists = true;
				}
			}
			if (!exists) break;
		}
		double latitude = LAT_MIN + Math.random() * (LAT_MAX-LAT_MIN);
		double longitude = LON_MIN + Math.random() * (LON_MAX-LON_MIN);
		String ipAddress = "127.0.0.1";
		System.out.println("Upisite port");
		int port = 1234;
		while(true) {
			port = input2.nextInt();
			if (port >= 1234 && port < 10000) {
				boolean exists = false;
				for (SensorDescription sd : sensors) {
					if (sd.getPort() == port) {
						System.out.println("Port zauzet");
						exists = true;
					}
				}
				if (!exists) break;
			}
		}
		SensorDescription sd = new SensorDescription();
		sd.setUsername(username);
		sd.setLatitude(latitude);
		sd.setLongitude(longitude);
		sd.setIpAddress(ipAddress);
		sd.setPort(port);
		rest.register(sd);
		sd.setId(rest.getSensorDescriptionId(sd.getUsername()));

		while(true) {
			System.out.println("Izvrsi mjerenje - 1\nCekaj zahtjev za otvaranje konekcije - 2\nIskljuci se - 0");
			int i = input2.nextInt();
			if (i == 1) {
				float broj_aktivnih_sekundi = System.currentTimeMillis() / 1000 - start_time / 1000;
				int redni_broj = (int)(broj_aktivnih_sekundi % 100) + 2;
//				System.out.println(broj_aktivnih_sekundi);
				System.out.println(readings.get(redni_broj));
				
				float[] values = new float[6];
				Measurement[] measurements = new Measurement[6];
				measurements[0] = new Measurement();	
				measurements[1] = new Measurement();	
				measurements[2] = new Measurement();	
				measurements[3] = new Measurement();	
				measurements[4] = new Measurement();	
				measurements[5] = new Measurement();	
				for (int counter = 0; counter < 6; counter++) {
										
					measurements[counter].setUsername(username);
				}
				values[0] = Float.parseFloat(readings.get(redni_broj).getTemperature());
				measurements[0].setParameter("temperature");
				values[1] = Float.parseFloat(readings.get(redni_broj).getHumidity());
				measurements[1].setParameter("pressure");
				values[2] = Float.parseFloat(readings.get(redni_broj).getPressure());
				measurements[2].setParameter("humidity");
				if (readings.get(redni_broj).getCo2().isEmpty()) values[3] = 0;
				else values[3] = Float.parseFloat(readings.get(redni_broj).getCo2());
				measurements[3].setParameter("co2");
				if (readings.get(redni_broj).getNo2().isEmpty()) values[4] = 0;
				else values[4] = Float.parseFloat(readings.get(redni_broj).getNo2());
				measurements[4].setParameter("no2");
				if (readings.get(redni_broj).getSo2().isEmpty()) values[5] = 0;
				else values[5] = Float.parseFloat(readings.get(redni_broj).getSo2());
				measurements[5].setParameter("so2");
				
				if (rest.getListOfSensors().size() > 1) {
					UserAddress userAddress = rest.searchNeighbour(sd.getUsername());
					System.out.println("Najblizi susjed ti je na adresi " + userAddress);
					
					// Spajanje na server
					Socket client = null;
					try {
						client = new Socket(userAddress.getIpAddress(), userAddress.getPort());					
					} catch (Exception e) {
						e.printStackTrace();
						rest.deleteSensorDescription(sd.getId());
					}
					BufferedReader inFromClient = new BufferedReader(new InputStreamReader(client.getInputStream()));
					PrintWriter outToClient = new PrintWriter(new OutputStreamWriter(client.getOutputStream()),true);
					outToClient.println("measure");
					String reading = inFromClient.readLine();
					
										
					
					System.out.println("dobiveni reading " + reading);
					System.out.println("moj reading " + readings.get(redni_broj));
					String[] measurementStrings = reading.split("\\[")[1].split("\\]")[0].split(",");
					for (int counter = 0; counter < 6; counter++) {
						String s = "";
						try {
							s = measurementStrings[counter].split("\\=")[1];
//							System.out.println(s);
						} catch (Exception e) {
//							e.printStackTrace();
						}
						if (s.isEmpty()) {
							if (values[counter] != 0) measurements[counter].setAverageValue(values[counter]);
							else measurements[counter].setAverageValue(0);
						} else {
							if (values[counter] != 0) measurements[counter].setAverageValue ((Float.parseFloat(s) + values[counter]) / 2);
							else measurements[counter].setAverageValue(Float.parseFloat(s));
						}
					}
				} else {
					for (int counter = 0; counter < 6; counter++) {
						measurements[counter].setAverageValue(values[counter]);
					}
				}
				for (int counter = 0; counter < 6; counter++) {
					if (measurements[counter].getAverageValue() != 0) {
						System.out.println("Spremam " + measurements[counter]);
						rest.storeMeasurement(measurements[counter]);
					}
				}
				System.out.println("Sva mjerenja " + rest.getMeasurements());
			} else if (i == 2) {
				ServerIf server = new MultiThreadedServer(start_time, sd.getPort());
				server.startup();
				server.loop();
				server.shutdown();
			} else if (i == 0) break;
		}
		rest.deleteSensorDescription(sd.getId());
		input2.close();
//		readings.stream().forEach(System.out::println);
	}
}