package hr.fer.rassus.klijent.urlconnection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import hr.fer.rassus.klijent.RestInterface;
import hr.fer.rassus.klijent.model.Measurement;
import hr.fer.rassus.klijent.model.SensorDescription;
import hr.fer.rassus.klijent.model.UserAddress;

public class UrlConnectionImplementation implements RestInterface {

	private String baseUrl;
	private ObjectMapper mapper;

	public UrlConnectionImplementation(String url) {
		this.baseUrl = url;
		mapper = new ObjectMapper();
	}

	private String loadData(String textURL) {
		try {
			URL url = new URL(textURL);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept", "application/json");
			if (connection.getResponseCode() != 200) {
				return "" + connection.getResponseCode();
			}

			InputStream contentStream = connection.getInputStream();
			String text = readTextFromStream(contentStream);
			connection.disconnect();
//			System.out.println("URL: ucitao sam: " + text);
			return text;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private String readTextFromStream(InputStream contentStream) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(contentStream));

		StringBuffer sb = new StringBuffer();
		String line = reader.readLine();
		while (line != null) {
			sb.append(line);
			sb.append("\n");
			line = reader.readLine();
		}
		String text = sb.toString();
		return text;
	}

	private HttpURLConnection writeData(String textURL, String method, String json) {
		try {
			URL url = new URL(textURL);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod(method);
			connection.setRequestProperty("Accept", "application/json");
			connection.setRequestProperty("Content-type", "application/json");
			connection.setDoOutput(true);
			OutputStream output = connection.getOutputStream();
			output.write(json.getBytes(StandardCharsets.UTF_8));
			output.close();

			connection.getResponseCode();

			return connection;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}


	@Override
	public List<SensorDescription> getListOfSensors() {
		try {
			String jsonText = loadData(baseUrl + "/getSensors");
//			System.out.println(jsonText);
			return mapper.readValue(jsonText, new TypeReference<List<SensorDescription>>(){});
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public SensorDescription getSensorDescription(int id) {
		try {
			String jsonText = loadData(baseUrl + "/getSensor/"+ id);
			System.out.println(jsonText);
			return mapper.readValue(jsonText, SensorDescription.class);
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}
	@Override
	public void register(SensorDescription sensorDescription) {
		try {
			String sensorJson = mapper.writeValueAsString(sensorDescription);
			System.out.println(sensorJson);
			writeData(baseUrl + "/newSensor", "POST", sensorJson);
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public int getSensorDescriptionId(String username) {
		try {
			String jsonText = loadData(baseUrl + "/getSensorId/"+ username);
//			System.out.println(jsonText);
			SensorDescription sd = mapper.readValue(jsonText, SensorDescription.class);
			return sd.getId();
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void storeMeasurement(Measurement measurement) {
		try {
			String sensorJson = mapper.writeValueAsString(measurement);
//			System.out.println(sensorJson);
			writeData(baseUrl + "/storeMeasurement", "POST", sensorJson);
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Measurement> getMeasurements() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteSensorDescription(int id) {
		// TODO Auto-generated method stub
	}

	@Override
	public UserAddress searchNeighbour(String username) {
		// TODO Auto-generated method stub
		return null;
	}
}
