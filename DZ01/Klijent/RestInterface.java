package hr.fer.rassus.klijent;

import java.util.List;

import hr.fer.rassus.klijent.model.Measurement;
import hr.fer.rassus.klijent.model.SensorDescription;
import hr.fer.rassus.klijent.model.UserAddress;

public interface RestInterface {

	List<SensorDescription> getListOfSensors();

	void register(SensorDescription sensorDescription);

	SensorDescription getSensorDescription(int id);

	int getSensorDescriptionId(String username);

	void storeMeasurement(Measurement measurement);

	List<Measurement> getMeasurements();

	void deleteSensorDescription(int id);

	UserAddress searchNeighbour(String username);
}
