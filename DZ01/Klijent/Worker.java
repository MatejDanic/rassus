package hr.fer.rassus.klijent;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import com.opencsv.bean.CsvToBeanBuilder;

import hr.fer.rassus.klijent.model.SensorReading;

public class Worker implements Runnable {
	private final Socket clientSocket;
	private final AtomicBoolean isRunning;
	private final AtomicInteger activeConnections;
	private final long start;
	
	public Worker(Socket clientSocket, AtomicBoolean
	isRunning, AtomicInteger activeConnections, long start) {
//		System.out.println("jesam li ovdje");
		this.clientSocket = clientSocket;
		this.isRunning = isRunning;
		this.activeConnections = activeConnections;
		this.start = start;
	}

	@Override
	public void run() {
//		System.out.println("a ovdje?");
		try (//create a new BufferedReader from an existing InputStream					
				BufferedReader inFromClient = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
				//create a PrintWriter from an existing OutputStream
				PrintWriter outToClient = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream()), true);) {
			
			String receivedString;
			// read a few lines of text
			while ((receivedString=inFromClient.readLine()) != null) {
				System.out.println("Server received:"+receivedString);
				//shutdown the server if requested
				if (receivedString.contains("shutdown")) {
					outToClient.println("Initiating server shutdown!");
					isRunning.set(false);
					activeConnections.getAndDecrement();
					return;
				} else if (receivedString.contains("measure")) {
					List<SensorReading> readings = new CsvToBeanBuilder<SensorReading>(new FileReader("mjerenja.csv")).withType(SensorReading.class).build().parse();
					float broj_aktivnih_sekundi = System.currentTimeMillis()/1000-start/1000;
					int redni_broj = (int)(broj_aktivnih_sekundi % 100) + 2;
					outToClient.println(readings.get(redni_broj));
				}
				String stringToSend = receivedString.toUpperCase();
				// send a String then terminate the line and flush
				outToClient.println(stringToSend);
				System.out.println("Server sent: " + stringToSend);
			}
			activeConnections.getAndDecrement();
		} catch (IOException ex) {
			System.err.println("Exception caught when trying to read or send data: " + ex);
		}
	}
}
