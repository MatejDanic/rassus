package hr.fer.rassus.klijent;

import hr.fer.rassus.klijent.RestInterface;
import hr.fer.rassus.klijent.resttemplate.RestTemplateImplementation;
import hr.fer.rassus.klijent.urlconnection.UrlConnectionImplementation;

public class RestFactory {

	public static RestInterface getUrlConnectionImplementation(String url) {
		return new UrlConnectionImplementation(url);
	}
	
	public static RestInterface getRestTemplateImplementation(String url) {
		return new RestTemplateImplementation(url);
	}

}
