package hr.fer.rassus.posluzitelj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class PosluziteljApplication {

	public static void main(String[] args) {
		SpringApplication.run(PosluziteljApplication.class, args);
	}

}
