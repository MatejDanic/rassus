package hr.fer.rassus.posluzitelj;

import org.springframework.data.jpa.repository.JpaRepository;

import hr.fer.rassus.posluzitelj.model.SensorDescription;

public interface SensorDescriptionRepository extends JpaRepository<SensorDescription, Integer> {
	
}
