package hr.fer.rassus.posluzitelj;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.rassus.posluzitelj.model.Measurement;
import hr.fer.rassus.posluzitelj.model.SensorDescription;
import hr.fer.rassus.posluzitelj.model.UserAddress;


@Service
public class SensorDescriptionService {

	@Autowired
	SensorDescriptionRepository repo;
	
	private static List<Measurement> measurements;

	public List<SensorDescription> getSensorDescriptions() {
		return repo.findAll();
	}

	public SensorDescription getSensorDescription(int id) {
		return repo.findById(id).get();
	}

	public void newSensorDescription(SensorDescription sensorDescription) {
//		System.out.println("aaaaaa" + sensorDescription); 
		repo.save(sensorDescription);
	}

	public void deleteSensorDescription(int id) {
		repo.deleteById(id);
	}

	public Optional<SensorDescription> getSensorDescriptionId(String username) {
		int value = 0;
		for (SensorDescription sd : repo.findAll()) {
			if (sd.getUsername().equals(username)) {
				value = sd.getId();
			}
		}
		return repo.findById(value);
	}

	public void initialize() {
		measurements = new LinkedList<Measurement>();
	}
	
	public void storeMeasurement(Measurement measurement) {
		int id = measurements.size();
		measurement.setId(id+1);
		measurements.add(measurement);
	}

	public List<Measurement> getMeasurements() {
		return measurements;
	}

	public UserAddress searchNeighbour(String username) {
		SensorDescription sensor1 = null;
		for (SensorDescription sd : repo.findAll()) {
			if (sd.getUsername().equals(username)) {
				sensor1 = sd;
			}
		}
		double lat1 = sensor1.getLatitude();
		double lon1 = sensor1.getLongitude();
		double r = 6371;
		double min = r;
		UserAddress userAddress = new UserAddress();
		for (SensorDescription sensor2 : repo.findAll()) {
			double lat2 = sensor2.getLatitude();
			double lon2 = sensor2.getLongitude();
			
			double dlon = lon2 - lon1;
			double dlat= lat2 - lat1;
			double a = Math.pow((Math.sin(dlat/2)),2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow((Math.sin(dlon/2)),2);
			double c = 2 * Math.atan2( Math.sqrt(a), Math.sqrt(1-a) );
			double d = r * c;
			if (d < min && d != 0) {
				min = d;
				userAddress.setIpAddress(sensor2.getIpAddress());
				userAddress.setPort(sensor2.getPort());
			}
		}
		return userAddress;
	}

}
