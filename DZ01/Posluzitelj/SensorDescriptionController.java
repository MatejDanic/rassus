package hr.fer.rassus.posluzitelj;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.rassus.posluzitelj.SensorDescriptionService;
import hr.fer.rassus.posluzitelj.model.Measurement;
import hr.fer.rassus.posluzitelj.model.SensorDescription;
import hr.fer.rassus.posluzitelj.model.UserAddress;

@RestController
@RequestMapping("/sensors")
public class SensorDescriptionController {
	
	@Autowired
	SensorDescriptionService service;
	
	public SensorDescriptionController(SensorDescriptionService service) {
		this.service = service;
		service.initialize();
	}
	
	@GetMapping("/getSensors")
	public List<SensorDescription> getSensors() {
		return service.getSensorDescriptions();
	}
	
	@GetMapping("/getSensor/{id}")
	public SensorDescription getSensor(@PathVariable(value="id") int id) {
		return service.getSensorDescription(id);
	}
	
	@GetMapping("/getSensorId/{username}")
	public Optional<SensorDescription> getSensor(@PathVariable(value="username") String username) {
		return service.getSensorDescriptionId(username);
	}
	
	@PostMapping("/newSensor")
	public void newSensor(@RequestBody SensorDescription sensorDescription) {
//		System.out.println("a" + sensorDescription);
		boolean exists = false;
		for (SensorDescription sd : service.getSensorDescriptions()) {
//			System.out.println(sd.getUsername() + "  ?  " + sensorDescription.getUsername());
			if (sd.getUsername().equals(sensorDescription.getUsername())) {
				exists = true;
			}
		}
		if (!exists) service.newSensorDescription(sensorDescription);
		System.out.println("Registriran senzor: " + sensorDescription);
	}
	
	@PostMapping("/storeMeasurement")
	public void storeMeasurement(@RequestBody Measurement measurement) {
		service.storeMeasurement(measurement);
		System.out.println("Pohranjeno mjerenje : " + measurement);
	}
	
	@GetMapping("/getMeasurements")
	public List<Measurement> getMeasurements() {
		return service.getMeasurements();
	}
	
	@DeleteMapping("/deleteSensor/{id}")
	public void deleteSensorDescription(@PathVariable(value="id") int id) {
		service.deleteSensorDescription(id);
		System.out.println("Iskljucen senzor " + service.getSensorDescription(id));
	}
	
	@GetMapping("/searchNeighbour/{username}")
	public UserAddress searchNeighbour(@PathVariable(value="username") String username) {
		UserAddress userAddress = new UserAddress();
		userAddress = service.searchNeighbour(username);
		System.out.println("Najblizi susjed od senzora " + username + " je senzor s adresom: " + userAddress);
		return userAddress;
	}
}
