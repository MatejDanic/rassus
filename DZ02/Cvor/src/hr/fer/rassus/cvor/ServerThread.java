/*
 * This code has been developed at Departement of Telecommunications,
 * Faculty of Electrical Engineering and Computing, University of Zagreb.
 */
package hr.fer.rassus.cvor;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Krešimir Pripužić <kresimir.pripuzic@fer.hr>
 */
public class ServerThread extends Thread{
    
    private int port; // server port
    public List<String> readings;
    public EmulatedSystemClock clock;
    Map<Long, Float> mapReadings;
    private int numOfPorts;
    public String counterVector;
	List<String> mapReadings2;
	private int ordinalNumber;
    
    public ServerThread(int port, EmulatedSystemClock clock, int numOfPorts, String counterVector) {
    	this.port = port;
    	this.readings = new LinkedList<>();
    	this.clock = clock;
    	mapReadings = new TreeMap<>();
    	mapReadings2 = new LinkedList<>();
    	this.numOfPorts = numOfPorts;
    	this.counterVector = counterVector;
    	ordinalNumber = port-1234;
    }

    /**
     * @param args the command line arguments
     */
    
	public void run() {
        byte[] rcvBuf = new byte[256]; // received bytes
        byte[] sendBuf = new byte[256];// sent bytes
        String rcvStr;

        // create a UDP socket and bind it to the specified port on the local
        // host
        DatagramSocket socket = null;
        

		try {
			socket = new SimpleSimulatedDatagramSocket(port, 0.2, 200);
		} catch (SocketException | IllegalArgumentException e2) {
			System.out.println("That port is already taken!");
			//e2.printStackTrace();
			Thread.currentThread().interrupt();
			return;
		} //SOCKET -> BIND

        while (true) { //OBRADA ZAHTJEVA
            // create a DatagramPacket for receiving packets
            DatagramPacket packet = new DatagramPacket(rcvBuf, rcvBuf.length);

            // receive packet
            try {
				socket.receive(packet);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

            // construct a new String by decoding the specified subarray of
            // bytes
            // using the platform's default charset
            rcvStr = new String(packet.getData(), packet.getOffset(),
                    packet.getLength());
//            System.out.println("Server received: " + rcvStr);
            Iterator<String> iterator = readings.iterator();
            boolean double_packet = false;
            while (iterator.hasNext()) {
            	String reading = iterator.next();
                if (reading.equals(rcvStr)) {
                	double_packet = true;
                }
            }
            if (!double_packet) {
            	for (int i = 0; i < numOfPorts; i++) {
        			if (i != port-1234) {
        				String[] vector = counterVector.split(",");
        				if (Integer.parseInt(vector[i]) < Integer.parseInt(rcvStr.split(" ")[1].split(",")[i]))
//        					System.out.println("moj vektor " + vector[i] + " je manji od " + rcvStr.split(" ")[1].split(",")[i]);
        					vector[i] = rcvStr.split(" ")[1].split(",")[i];
        				counterVector = "";
        				for (String s : vector) {
        					counterVector += s+",";
        				}
        			} else {
        				String[] vector = counterVector.split(",");
        				vector[i] = (Integer.parseInt(vector[i])+1)+"";
        				counterVector = "";
        				for (String s : vector) {
        					counterVector += s+",";
        				}
        			}
        		}
            	readings.add(rcvStr);
            }
            long diff = clock.currentTimeMillis() - Long.parseLong(rcvStr.split(" ")[0]);
            if (diff < 0) {
    			clock.difference(-diff + 1000);
//    			System.out.println("Adjusting clock");
            }
            updateReadings();
            // encode a String into a sequence of bytes using the platform's
            // default charset
            sendBuf = (rcvStr).getBytes();
//            System.out.println("Server sends: " + rcvStr.toUpperCase());

            // create a DatagramPacket for sending packets
            DatagramPacket sendPacket = new DatagramPacket(sendBuf,
                    sendBuf.length, packet.getAddress(), packet.getPort());
            // send packet
            try {
				socket.send(sendPacket);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} //SENDTO
        }
    }
	public void updateReadings() {
		mapReadings.clear();
		mapReadings2.clear();
        for (String reading : readings) {
        	if (clock.currentTimeMillis() - Long.parseLong(reading.split(" ")[0]) <= 5000) {
        		mapReadings.put(Long.parseLong(reading.split(" ")[0]), Float.parseFloat(reading.split(" ")[2]));
        		mapReadings2.add(reading);
        	}
        }
//        System.out.println(mapReadings2);
		java.util.Collections.sort(mapReadings2, new java.util.Comparator<String>() {
		    @Override
		    public int compare(String s1, String s2) {
		    	String v1 = s1.split(" ")[1];
		    	String v2 = s2.split(" ")[1];
//		    	System.out.println("v1:" + v1 + ", v2: "+ v2);
		    	return Integer.parseInt(v1.split(",")[ordinalNumber]) - Integer.parseInt(v2.split(",")[ordinalNumber]);
		    }  
		});
	}
}
