package hr.fer.rassus.cvor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Scanner;


public class CvorApplication {
	
	public static String counterVector;
	public static EmulatedSystemClock clock;
	
	public static void main(String[] args) throws NumberFormatException, IOException, InterruptedException {
		
		counterVector = "";
		//READING ALL PORTS
		File ports = new File("ports.txt");
		BufferedReader br = new BufferedReader(new FileReader(ports));
		List<Integer> portList = new LinkedList<>();
		String st;
		while((st = br.readLine()) != null) {
			portList.add(Integer.parseInt(st));
		}
		br.close();
		System.out.println(portList);
		
		
		//SELECTING FROM AVAILABLE PORTS
		ServerThread serverThread = null;
		Scanner input = new Scanner(System.in);
		int receivePort = 0;
		clock = new EmulatedSystemClock();
		while (true) {
			System.out.println("Enter server port: ");
			try {
				receivePort = Integer.parseInt(input.nextLine());
			} catch (NumberFormatException e ) {
				System.out.println("Try again.");
				continue;
			}
			
			if (portList.contains(receivePort)) {
				int ordinalNumber = receivePort-1234;
				counterVector ="";
				for (int i = 0; i < portList.size(); i++) {
					counterVector += "0,";
				}
				String[] vector = counterVector.split(",");
				vector[ordinalNumber] = "0";
				counterVector = "";
				for (String s : vector) {
					counterVector += s+",";
				}
				
				System.out.println(counterVector);
				
				serverThread = new ServerThread(receivePort, clock, portList.size(), counterVector);
				serverThread.start();
				
				Thread.sleep(500);
				if(serverThread.isAlive()) break;
			} else {
				System.out.println("Port unavailable!");
			}
		}
		input.close();
		int ordinalNumber = receivePort-1234;
//		System.out.println("Your port is: " + receivePort);
		long start_time = clock.currentTimeMillis();
		long current_time = start_time;
		long active_time = current_time - start_time;
		int counter = 0;
		
		//SENDING MESSAGE TO ALL PORTS
    	String sendString = "";
    	File mjerenja = new File("mjerenja.csv");
    	br = new BufferedReader(new FileReader(mjerenja));
		List<String> readings = new LinkedList<>();
		String obj;
		boolean first = true;
		while((obj = br.readLine()) != null) {
			if (first) {
				first = false;
				continue;
			}
			readings.add(obj);
		}
		br.close();
		List<Integer> coReadings = new LinkedList<>();
		for (String r : readings) {
			try {
				coReadings.add(Integer.parseInt(r.split(",")[3]));
			} catch (NumberFormatException e) {
				System.out.println("a");
			}
		}
		System.out.println("\nStarting the measuring...");
		
    	while(true) {
    		counter++; 
			counterVector = serverThread.counterVector;
    		String[] vector = counterVector.split(",");
			vector[ordinalNumber] = ""+counter;
			counterVector = "";
			for (String s : vector) {
				counterVector += s+",";
			}
    		
    		Thread.sleep(1000);
    		current_time = clock.currentTimeMillis();
    		active_time = current_time - start_time;
    		sendString = coReadings.get((int)(((Math.abs(active_time)/1000)%100))).toString();
    		System.out.println("Time: " + (current_time/1000)%10000 + ", Vector timestamp: " + counterVector + " -> Value: " + sendString);

    		
//    		System.out.println(counterVector);
    		serverThread.readings.add(current_time + " " + counterVector + " " + sendString);
    		serverThread.updateReadings();
    		
    		
       		for (int port : portList) {
       			if (port == receivePort) continue;
       			
       			//povecavanje svog countera za svako slanje?
//       			vector = counterVector.split(",");
//    			vector[ordinalNumber] = "" + ++counter;
//    			counterVector = "";
//    			for (String s : vector) {
//    				counterVector += s+",";
//    			}
       			KlijentThread kt = new KlijentThread(port, current_time + " " + counterVector + " " + sendString);
       			kt.start();
       			Thread.sleep(100);
       		}
    		if (counter % 5 == 0) {
    			System.out.println("\nSCALAR TIMESTAMPS\nReadings in last 5 seconds, current time: " + current_time/1000%10000);
    			int sum = 0;
    			for (Entry<Long, Float> entry : serverThread.mapReadings.entrySet()) {
    	            System.out.println("Time: " + entry.getKey()/1000%10000 + ", Value: " + entry.getValue()); 
    				sum += entry.getValue();
    			}
    			
    			System.out.println("\nVECTOR TIMESTAMPS\nReadings in last 5 seconds, current time: " + current_time/1000%10000);
    		
    			for (String entry : serverThread.mapReadings2) {
    	            System.out.println("Timestamp: " + entry.split(" ")[1] + " -> Value: " + Float.parseFloat(entry.split(" ")[2])); 
    				
    			}
    			if (serverThread.mapReadings.size() > 0) 
    				System.out.println("\nAverage reading: " + sum/serverThread.mapReadings.size());
    		}
    	}
   	}
}
