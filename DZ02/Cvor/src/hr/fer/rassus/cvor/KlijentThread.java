/*
 * This code has been developed at Departement of Telecommunications,
 * Faculty of Electrical Eengineering and Computing, University of Zagreb.
 */
package hr.fer.rassus.cvor;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Krešimir Pripužić <kresimir.pripuzic@fer.hr>
 */
public class KlijentThread extends Thread{

	private static final int COUNTER_LIMIT = 4;
	private int counter = 0;
	private int port;
	private String sendString;
	
	public KlijentThread(int port, String sendString) {
		this.port = port;
		this.sendString = sendString;
	}

    public void run() {
    	StringBuffer receiveString = new StringBuffer();
    	
    	
    		
		//String sendString = "Any string...";
		byte[] rcvBuf = new byte[256]; // received bytes
		// encode this String into a sequence of bytes using the platform's
		// default charset and store it into a new byte array
		
		// determine the IP address of a host, given the host's name
		InetAddress address = null;
		try {
			address = InetAddress.getByName("localhost");
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
			// create a datagram socket and bind it to any available
		// port on the local host
		//DatagramSocket socket = new SimulatedDatagramSocket(0.2, 1, 200, 50); //SOCKET
		DatagramSocket socket = null;
		try {
			socket = new SimpleSimulatedDatagramSocket(0.2, 200);
		} catch (SocketException | IllegalArgumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} //SOCKET
		
		while (receiveString.toString().isEmpty()) {
//    		System.out.print(counter +". Client sends to port: " + port + " -> ");
			byte[] sendBuf = new byte[sendString.length()];// sent bytes
			for (int i = 0; i < sendBuf.length; i++) {
				sendBuf[i] = (byte) sendString.charAt(i);
			}
			// create a datagram packet for sending data
			DatagramPacket packet = new DatagramPacket(sendBuf, sendBuf.length,
					address, port);
			// send a datagram packet from this socket
			try {
				socket.send(packet);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} //SENDTO
//			System.out.print(new String(sendBuf));
    		// send each character as a separate datagram packet
			
//    		System.out.println("");
    		while (true) {
    			// create a datagram packet for receiving data
    			DatagramPacket rcvPacket = new DatagramPacket(rcvBuf, rcvBuf.length);
    			try {
    				// receive a datagram packet from this socket
    				socket.receive(rcvPacket); //RECVFROM
    			} catch (SocketTimeoutException e) {
    				break;
    			} catch (IOException ex) {
    				Logger.getLogger(CvorApplication.class.getName()).log(Level.SEVERE, null, ex);
    			}
    			// construct a new String by decoding the specified subarray of bytes
    			// using the platform's default charset
    			receiveString.append(new String(rcvPacket.getData(), rcvPacket.getOffset(), rcvPacket.getLength()));
    		}
    		counter++;
    		if (counter == COUNTER_LIMIT) {
//    			System.out.println("Counter for: " + sendString + " reached its limit, sending stopped!");
    			break;
    		} else {
//    			System.out.println("No confirmation, sending " + sendString + " again...");
    		}
		}
		socket.close(); //CLOSE
		if (!receiveString.toString().isEmpty()) {
//			System.out.println("Client received from port: " + port + " -> " + receiveString);
		}

		// close the datagram socket
    }
}