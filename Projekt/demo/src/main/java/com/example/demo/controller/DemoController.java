package com.example.demo.controller;

import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

@RestController
class DemoController{

	@Autowired
	RestTemplate restTemplate;
	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}
	
	private static final Logger LOG = Logger.getLogger(DemoController.class.getName());

	@GetMapping(value="/demo")
	public String demoService1() {
		LOG.info("Inside demo service 1...");
		String response = "Visited demo service 1.\n";

		try {
			response += (String) restTemplate.exchange("http://localhost:8082/demo2", HttpMethod.GET, null, new ParameterizedTypeReference<String>() {
			}).getBody();
		} catch (ResourceAccessException e) {
			LOG.info(e.getLocalizedMessage());
		}
		return response;
	}
}
